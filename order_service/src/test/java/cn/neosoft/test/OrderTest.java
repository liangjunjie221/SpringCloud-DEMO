package cn.neosoft.test;

import cn.neosoft.order_service.dto.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class OrderTest {
    @Test
    public void testOrder(){
        Order order = new Order();
        order.setUserid("1");
        order.setProduct_name("AMXL");
        order.setProductid("SPH001002003");
        order.setCreatetime(new Date());
        order.setOrderid(10010);
        order.setPrice(12.01);
        order.setUsername("用户名");
        Assertions.assertEquals(order,order);
    }
}
