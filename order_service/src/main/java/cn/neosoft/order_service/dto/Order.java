package cn.neosoft.order_service.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.time.DateUtils;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@Data
public class Order implements Serializable {

    private int orderid;

    private String product_name;

    private String productid;

    private Double price;

    private Date createtime;

    private String userid;

    private  String username;

}
