package cn.neosoft.order_service.controller;

import cn.neosoft.order_service.dto.Order;
import cn.neosoft.order_service.service.OrderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    private static final Log log = LogFactory.getLog(OrderController.class);

    @RequestMapping("save")
    @HystrixCommand(fallbackMethod = "fallbackMethod")
    public Object save(@RequestParam("user_id") String userid, @RequestParam("product_id") String productid, HttpServletRequest request) throws JsonProcessingException {

        log.info("开始进行保存");

        String token = request.getHeader("token");
        String cookie = request.getHeader("cookie");

        System.out.println(token+"===================="+cookie);

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("code",0);
        Order order =orderService.save(userid,productid);
        map.put("data",order);

        log.info("保存完毕");

        return map;
    }

    public Object fallbackMethod(String userid,String productid,HttpServletRequest request){

        //服务熔断降级通知

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("code",-1);
        map.put("msg","系统访问量巨大请稍后再试!");
        return map;
    }
}
