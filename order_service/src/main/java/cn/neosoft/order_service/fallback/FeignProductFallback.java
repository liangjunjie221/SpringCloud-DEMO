package cn.neosoft.order_service.fallback;

import cn.neosoft.order_service.service.FeignProductService;
import org.springframework.stereotype.Component;

@Component
public class FeignProductFallback implements FeignProductService {

    @Override
    public String findById(String id) {
        System.out.println("SYSTEM 发生降级事件!");
        return null;
    }

}
