package cn.neosoft.order_service.service;

import cn.neosoft.order_service.dto.Order;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface OrderService {

    public Order save(String userid, String productid) throws JsonProcessingException;
}
