package cn.neosoft.order_service.service;

import cn.neosoft.order_service.fallback.FeignProductFallback;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "oms-service",fallback = FeignProductFallback.class)
public interface FeignProductService {

    @GetMapping("/api/v1/product/findbyid")
    public String findById(@RequestParam String id);
}
