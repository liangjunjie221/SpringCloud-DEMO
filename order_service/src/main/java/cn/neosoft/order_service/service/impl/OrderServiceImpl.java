package cn.neosoft.order_service.service.impl;

import cn.neosoft.order_service.dto.Order;
import cn.neosoft.order_service.service.FeignProductService;
import cn.neosoft.order_service.service.OrderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Date;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private FeignProductService feignProductService;

    @Autowired
    private LoadBalancerClient loadBalancer;

    @Override
    public Order save(String userid, String productid) throws JsonProcessingException {

        //获取商品服务接口中的数据利用ribon
        //Object obj = restTemplate.getForObject("http://product-service/api/v1/product/findbyid?id="+productid,Object.class);
        //Map<String,Object> obj = restTemplate.getForObject("http://product-service/api/v1/product/findbyid?id="+productid,Map.class);
        //System.out.println(obj);

        /*ServiceInstance instance = loadBalancer.choose("product-service");
        String url =String.format("http://%s:%s", instance.getHost(), instance.getPort());
        System.out.println(url);
        Map<String,Object> obj = restTemplate.getForObject(url,Map.class);*/

        //String resultstr=feignProductService.findById(productid);

        //System.out.println(resultstr);

        //string转jsonNode
        //ObjectMapper objectMapper = new ObjectMapper();
        //JsonNode jsonNode=objectMapper.readTree(resultstr);

        Order order = new Order();
        order.setCreatetime(new Date());
        order.setUserid(userid);
        order.setProductid(productid);
        order.setProduct_name("阿莫西林");

        return order;
    }
}
