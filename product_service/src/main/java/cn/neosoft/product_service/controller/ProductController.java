package cn.neosoft.product_service.controller;

import cn.neosoft.product_service.dto.Product;
import cn.neosoft.product_service.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api/v1/product")
@RefreshScope
public class ProductController {

    /*@Value("${env}")
    private String env;*/

    @Autowired
    private ProductService productService;

    @RequestMapping("list")
    public Object getProductList(){
        return productService.listProducts();
    }

    @RequestMapping("findbyid")
    public Object findById(int id){

        if(id==2){
            //模拟超时的情况
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Product product = productService.findByID(id);
        Product product_new = new Product();
        BeanUtils.copyProperties(product,product_new);
        return product_new;
    }
}
