package cn.neosoft.product_service.service.impl;

import cn.neosoft.product_service.dto.Product;
import cn.neosoft.product_service.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Override
    public List<Product> listProducts() {

        List<Product> products = new ArrayList<Product>();

        Product product1= new Product();
        product1.setId(1);
        product1.setName("阿莫西林");
        product1.setPrice(200.01);
        product1.setUnit("盒");

        Product product2= new Product();
        product2.setId(2);
        product2.setName("奥美拉唑");
        product2.setPrice(100.91);
        product2.setUnit("盒");

        products.add(product1);
        products.add(product2);

        return products;
    }

    @Override
    public Product findByID(int id) {
        Product product1= new Product();
        product1.setId(1);
        product1.setName("阿莫西林");
        product1.setPrice(200.01);
        product1.setUnit("盒");
        return product1;
    }
}
