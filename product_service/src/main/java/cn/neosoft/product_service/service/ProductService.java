package cn.neosoft.product_service.service;

import cn.neosoft.product_service.dto.Product;

import java.util.List;

public interface ProductService {

    List<Product> listProducts();

    Product findByID(int id);

}
